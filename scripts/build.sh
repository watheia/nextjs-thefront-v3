#!/usr/bin/env bash

set -e
set -u
set -o pipefail

readonly PROGDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
readonly PROJECTDIR="$(cd "${PROGDIR}/.." && pwd)"

# shellcheck source=SCRIPTDIR/.util/tools.sh
source "${PROGDIR}/.util/tools.sh"

# shellcheck source=SCRIPTDIR/.util/print.sh
source "${PROGDIR}/.util/print.sh"

function main() {
    name="$(basename "${PROJECTDIR}")"
    util::print::info "%s" "Building ${name}..."

    # TODO use utils to ensure pack is installed
    pack build registry.digitalocean.com/watheia/materials/front-typescript-nextjs-30 \
        --builder paketobuildpacks/builder:full \
        --buildpack gcr.io/paketo-buildpacks/nodejs \
        --path "${PROJECTDIR}"

    util::print::info "Success!"
}

main "${@:-}"
