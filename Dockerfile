FROM node:lts-alpine as build

WORKDIR /wa
ADD . .
RUN yarn install && \
  yarn next build && \
  yarn next export

FROM node:lts-alpine

COPY --from=build /wa/out /var/www/html
RUN npm install --global serve
EXPOSE 5000

CMD [ "serve", "/var/www/html" ]
